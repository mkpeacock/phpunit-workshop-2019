<?php

namespace Workshop\Monsters;

use Workshop\Monsters\Models\User;

class Mailer
{
    public function sendNewUserRegistrationEmail(User $user)
    {
        throw new \LogicException('An email has been triggered. We do not want to do this from a test.');
    }
}
