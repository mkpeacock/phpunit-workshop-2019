<?php

namespace Workshop\Monsters;

class FirstUnit
{
    public function returnsTrue()
    {
        return true;
    }

    public function inverseBoolean(bool $input): bool
    {
        return ! $input;
    }

    public function formatLicenseKey(string $key)
    {
        return trim(chunk_split($key, 4, '-'));
    }

    public function integerBiggerThan(int $number)
    {
        return $number + 1;
    }

    public function integerSmallerThan(int $number)
    {
        return $number - 1;
    }

    public function fizzBuzz(int $number)
    {
        $return = [];

        if (0 === $number % 3) {
            $return[] = 'Fizz';
        }

        if (0 === $number % 5) {
            $return[] = 'Buzz';
        }

        if (empty($return)) {
            return $number;
        }

        return implode($return);
    }

    public function fizzBuzzTo(int $number)
    {
        $output = [];

        for ($i = 1; $i < $number; $i++) {
            $output[] = $this->fizzBuzz($number);
        }

        return $output;
    }
}
