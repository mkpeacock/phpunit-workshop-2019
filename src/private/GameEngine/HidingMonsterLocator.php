<?php

namespace Workshop\Monsters\GameEngine;

use Illuminate\Support\Collection;
use Workshop\Monsters\Models\Monster;
use Workshop\Monsters\Repositories\HidingPlaceRepository;
use Workshop\Monsters\Repositories\MonsterRepository;

class HidingMonsterLocator
{
    public function __construct(MonsterRepository $monsterRepository, HidingPlaceRepository $hidingPlaceRepository)
    {
        $this->monsterRepository = $monsterRepository;
        $this->hidingPlaceRepository = $hidingPlaceRepository;
    }

    public function findHidingMonsters()
    {
        $hidingMonsters = new Collection();

        foreach ($this->monsterRepository->getAllMonsters() as $monster) {
            if ($this->hidingPlaceRepository->getHidingPlacesForMonster($monster)->count() > 0) {
                $hidingMonsters->push($monster);
            }
        }

        return $hidingMonsters;
    }

    public function findHidingMonstersAlternative()
    {
        $monsters = $this->monsterRepository->getAllMonsters();
        $hidingPlaces = $this->hidingPlaceRepository->getAllHidingPlaces();

        return $monsters->filter(function (Monster $monster) use ($hidingPlaces) {
            foreach ($hidingPlaces as $hidingPlace) {
                if ($hidingPlace->monster_id === $monster->id) {
                    return true;
                }
            }

            return false;
        });
    }
}
