<?php

namespace Workshop\Monsters\Models;

use Illuminate\Database\Eloquent\Model;
use Workshop\Monsters\HasName;

class User extends Model
{
    use HasName;

    protected $table = 'users';

    public $timestamps = false;

    public function monsters()
    {
        return $this->hasMany(Monster::class, 'owner_id');
    }
}
