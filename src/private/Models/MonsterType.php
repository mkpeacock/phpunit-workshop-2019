<?php

namespace Workshop\Monsters\Models;

use Illuminate\Database\Eloquent\Model;

class MonsterType extends Model
{
    protected $table = 'monster_types';

    public $timestamps = false;

    public function monsters()
    {
        return $this->hasMany(Monster::class);
    }

    public function breed()
    {
        return $this->belongsTo(MonsterBreed::class, 'monster_breed_id');
    }
}
