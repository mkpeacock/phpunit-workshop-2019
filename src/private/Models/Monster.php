<?php

namespace Workshop\Monsters\Models;

use Illuminate\Database\Eloquent\Model;

class Monster extends Model
{
    protected $table = 'monsters';

    public $timestamps = false;

    public function type()
    {
        return $this->belongsTo(MonsterType::class, 'monster_type_id');
    }

    public function owner()
    {
        return $this->belongsTo(User::class, 'owner_id');
    }
}
