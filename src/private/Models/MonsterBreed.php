<?php

namespace Workshop\Monsters\Models;

use Illuminate\Database\Eloquent\Model;

class MonsterBreed extends Model
{
    protected $table = 'monster_breeds';

    public $timestamps = false;

    public function types()
    {
        return $this->hasMany(MonsterType::class, 'monster_breed_id');
    }
}
