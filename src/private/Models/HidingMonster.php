<?php

namespace Workshop\Monsters\Models;

use Illuminate\Database\Eloquent\Model;

class HidingMonster extends Model
{
    protected $table = 'hiding_monsters';

    public $timestamps = false;

    public function monster()
    {
        return $this->belongsTo(Monster::class);
    }
}
