<?php

namespace Workshop\Monsters;

use Workshop\Monsters\Models\User;
use Workshop\Monsters\Repositories\UserRepository;

class UserRegistrationHandler
{
    /**
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * UserRegistrationHandler constructor.
     * @param UserRepository $userRepository
     * @param Mailer $mailer
     */
    public function __construct(UserRepository $userRepository, Mailer $mailer)
    {
        $this->userRepository = $userRepository;
        $this->mailer = $mailer;
    }

    public function registerUser(string $name, string $email, string $password): User
    {
        // Check for existing user
        if (null !== $this->userRepository->findByEmailAddress($email)) {
            throw new \RuntimeException('User with that email already exists');
        }

        // Create user
        $user = $this->userRepository->newInstance();
        $user->name = $name;
        $user->email = $email;
        $user->password = password_hash($password, \PASSWORD_DEFAULT);
        $user->save();

        // Send email
        $this->mailer->sendNewUserRegistrationEmail($user);

        // Return user
        return $user;
    }
}
