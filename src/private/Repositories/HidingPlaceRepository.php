<?php

namespace Workshop\Monsters\Repositories;

use Workshop\Monsters\Models\HidingMonster;
use Workshop\Monsters\Models\Monster;

class HidingPlaceRepository extends AbstractRepository
{
    public function __construct(HidingMonster $model)
    {
        parent::__construct($model);
    }

    public function getHidingPlacesForMonster(Monster $monster)
    {
        return $this->model->where('monster_id', '=', $monster->id)->get();
    }

    public function getAllHidingPlaces()
    {
        return $this->model->all();
    }
}
