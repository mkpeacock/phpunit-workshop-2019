<?php

namespace Workshop\Monsters\Repositories;

use Workshop\Monsters\Models\User;

class UserRepository extends AbstractRepository
{
    public function __construct(User $model)
    {
        parent::__construct($model);
    }

    public function findByEmailAddress(string $email)
    {
        return $this->model->where('email', '=', $email)->first();
    }
}
