<?php

namespace Workshop\Monsters\Repositories;

use Workshop\Monsters\Models\Monster;

class MonsterRepository extends AbstractRepository
{
    public function __construct(Monster $model)
    {
        parent::__construct($model);
    }

    public function getAllMonsters()
    {
        return $this->model->all();
    }
}
