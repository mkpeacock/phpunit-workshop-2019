<?php

namespace Workshop\Monsters;

use Workshop\Monsters\Models\User;

class UserRegistrationHandlerHardToTest
{
    public function registerUser(string $name, string $email, string $password): User
    {
        // Check for existing user
        if (null !== User::where('email', '=', $email)->first()) {
            throw new \RuntimeException('User with that email already exists');
        }

        // Create user
        $user = new User();
        $user->name = $name;
        $user->email = $email;
        $user->password = password_hash($password, \PASSWORD_DEFAULT);
        $user->save();

        // Send email
        (new Mailer())->sendNewUserRegistrationEmail($user);

        // Return user
        return $user;
    }
}
