<?php

namespace Workshop\Monsters;

trait HasName
{
    public function getName()
    {
        return $this->name;
    }
}
