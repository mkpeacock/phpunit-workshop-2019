<?php

namespace Workshop\Monsters;

class FizzBuzz
{
    public function fizzBuzz(int $number)
    {
        $return = [];

        if (0 === $number % 3) {
            $return[] = 'Fizz';
        }

        if (0 === $number % 5) {
            $return[] = 'Buzz';
        }

        if (empty($return)) {
            return $number;
        }

        return implode($return);
    }

    public function fizzBuzzTo(int $number)
    {
        $output = [];

        for ($i = 1; $i < $number; $i++) {
            $output[] = $this->fizzBuzz($number);
        }

        return $output;
    }
}
