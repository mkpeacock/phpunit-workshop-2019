<?php

use Phinx\Migration\AbstractMigration;

class InitialData extends AbstractMigration
{
    public function up()
    {
        $sql = "INSERT INTO `monster_breeds` (`id`, `breed`) VALUES (1, 'Water Based'), (2, 'Fire Based'), (3, 'Earth Based')";
        $this->execute($sql);

        $sql = "INSERT INTO `monster_types` (`id`, `name`, `monster_breed_id`) VALUES (1, 'Kraken', 1), (2, 'Dragon', 2), (3, 'Giant Worm', 3)";
        $this->execute($sql);

        $passwordHash = password_hash('password1234', \PASSWORD_DEFAULT);
        $sql = "INSERT INTO `users` (`id`, `name`, `email`, `password`) VALUES (1, 'Demo', 'test@test.com', '" . $passwordHash . "')";
        $this->execute($sql);

        $sql = "INSERT INTO `monsters` (`id`, `name`, `monster_type_id`, `owner_id`) VALUES (1, 'Fred', 1, NULL), (2, 'George', 2, 1), (3, 'Bill', 3, NULL)";
        $this->execute($sql);

        $sql = "INSERT INTO `hiding_monsters` (`id`, `monster_id`, `latitude`, `longitude`) VALUES (1, 1, '54.964006', '-1.605754'), (2, 3, '51.5085300', '-0.1257400')";
        $this->execute($sql);
    }

    public function down()
    {
        $this->execute("DELETE FROM `hiding_monsters`");
        $this->execute("DELETE FROM `monsters`");
        $this->execute("DELETE FROM `users`");
        $this->execute("DELETE FROM `monster_types`");
        $this->execute("DELETE FROM `monster_breeds`");
    }
}
