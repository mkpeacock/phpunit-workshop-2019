<?php

use Phinx\Migration\AbstractMigration;

class Monsters extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $users = $this->table('users');
        $users->addColumn('name', 'string', ['null' => false]);
        $users->addColumn('email', 'string', ['null' => false]);
        $users->addColumn('password', 'string', ['null' => false]);
        $users->addIndex(['email'], ['unique' => true]);
        $users->save();

        $monsterBreeds = $this->table('monster_breeds');
        $monsterBreeds->addColumn('breed', 'string', ['null' => false]);
        $monsterBreeds->save();

        $monsterTypes = $this->table('monster_types');
        $monsterTypes->addColumn('name', 'string', ['null' => false]);
        $monsterTypes->addColumn('monster_breed_id', 'integer', ['null' => false]);
        $monsterTypes->addForeignKey('monster_breed_id', 'monster_breeds', 'id');
        $monsterTypes->save();

        $monsters = $this->table('monsters');
        $monsters->addColumn('name', 'string');
        $monsters->addColumn('monster_type_id', 'integer', ['null' => false]);
        $monsters->addForeignKey('monster_type_id', 'monster_types', 'id');
        $monsters->addColumn('owner_id', 'integer', ['null' => true, 'default' => null]);
        $monsters->addForeignKey('owner_id', 'users', 'id');
        $monsters->save();

        $hidingMonsters = $this->table('hiding_monsters');
        $hidingMonsters->addColumn('monster_id', 'integer', ['null' => false]);
        $hidingMonsters->addForeignKey('monster_id', 'monsters', 'id');
        $hidingMonsters->addColumn('latitude', 'decimal', ['precision' => 9, 'scale' => 6]);
        $hidingMonsters->addColumn('longitude', 'decimal', ['precision' => 9, 'scale' => 6]);
        $hidingMonsters->addIndex(['latitude', 'longitude']);
        $hidingMonsters->save();
    }
}
